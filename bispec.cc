/*
 * Calculate the bispectrum for a 3D box
 * of type float and organised into a 1D array according to the following
 * indexing for (x,y,z) box co-ordinates: i = z + D*(y+D*x), 
 * where i is the 1D index and D is the pixel count per box side.
 *
 */

#include <iostream> 
#include "math.h"

#include "../Reio/TOCMFAST.h"
#include "BISPEC.h"


/* wrapper managing bispectrum calculations*/
int calc_bispec(char *file_, int res_, int boxtype)
{
  /* Create an array with m rows, each one storing the
     index for every mode whose length is ~m*/
  fftwf_complex *box_fft;
  std::vector< std::vector<int> > ind_hold;
  float avg;

  // Fourier transform box
  box_fft = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);
  
  if ( !(import_as_fft(file_, res_, boxtype, box_fft)) )
    {
      fprintf(stderr, "bispec.cc >> calc_bispec -> import_as_fft (tocmfast.cc) has returned an error\n... Aborting \n");
      return -1;
    }
  if (!box_fft){
    fprintf(stderr, "bispec.cc >> calc_bispec -> Error importing FFT box\n... Aborting \n");
    return -1;
  }

  // generate the holding array
  if (!make_ind_hold(box_fft, ind_hold, res_, 1) )
    {
      fprintf(stderr, "Error in bispec.cc << make_ind_hold.\n");
      return -1;
    }
  std::cout<<ind_hold.size()<<std::endl;
  for (int i=0; i<ind_hold.size(); i++)
    {
      return 1;
    }
  free(box_fft);
  return 1;
}

/*This constructs a vector of m vectors, each row containing
a vector of the 1D index locations of all entries in box
that meet the requirement |m-n|<1/2, where n is the length
of a vector from the 3D box.
This is acheived in one pass through the box using
a vector (length m) of vectors (built up 
dynamically using push_back)
*/
int make_ind_hold(fftwf_complex *box_, std::vector< std::vector<int> > &ind_hold, int res_, int s)
{
  /*
    box contains data, res is the resolution (per side) of said data
    and s is the user choice for binwidth of the bispectrum
    calculations, i.e. dk = s*k_f where k_f is the fundamental
    k 2pi/L
    
    Fills up ind_hold with m rows, each one containing a vector of 
    every pixel in the box with sqrt(x^2 + y^2 + z^2) ~ m

    !!! NOTE: currently only set up for s=1 
   */
  //std::vector< std::vector<int> > ind_hold;
  unsigned long long m_max, index;

  if (s>1)
    {
      fprintf(stderr, "bispec.cc >>make_ind_hold  -> Not yet set up for anything other than s=1; overriding user choice.... GUTTED!\n");
      s=1;
    }
  
  m_max = sqrt(3.0*pow(res_,3));
  std::cout<<m_max<<std::endl;
  // Create a vector of vectors and reserve some memory to reduce the impact of rellocations
  for (unsigned long long i=0; i<(m_max+1);i++)
    {
      ind_hold.push_back(std::vector<int>()); // creates an empty vector-row in ind_hold[i]
      ind_hold[i].reserve(res_/2.0); //reserves memory for the vector-row to reduce reallocations
      //std::cout<<'i='<<i<<'\t'<<ind_hold[i].capacity()<<std::endl;
    }
  for  (int n_x=0; n_x<res_; n_x++)
    {
      for  (int n_y=0; n_y<res_; n_y++)
	{
	  for  (int n_z=0; n_z<res_; n_z++)
	    {
	      index = int( sqrt(pow(n_x, 2) + pow(n_y, 2) + pow(n_z, 2)) + 0.5 );
	      ind_hold[index].push_back( HII_C_INDEX(n_x, n_y, n_z) );
	    }  
	}     
    }
  std::cout<<ind_hold.size()<<std::endl;
	      

  return 1;
}
