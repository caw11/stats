#ifndef _STAT3PT_H
#define _STAT3PT_H


/* REFERENCES:
 * Furlanetto, Zaldarriaga & Hernquist 2004 (hereafter FZH04)
 * Watkinson & Pritchard (in prep)
 * Powell 
 */

#include <math.h>
#include <stdlib.h>
//#include <string.h>
#include <fstream> 
#include <iostream> 
#include "../USERFLAGS.h"
#include "../Reio/TOCMFAST.h"
#include "../Reio/REIONISE.h"

const int inclClus(0); // to include the effects of clustering (only implemented in the 2 point correlation routines)

class Stat3pt {

 public:
  //CLASS INITIALISATION/DESTRUCTION
  Stat3pt(FILE *LOG, float *box, int res, float l, int boxtype, Tocmfast *T1, Reionise *R1, Cosmology *c1);
  ~Stat3pt();


  //ANALYTICAL xH 3-POINT CORRELATION FN, see section 3.2 of FZH04
  double P3pt_xH(double a, double b, double c);

/*************************************************************************
    PROBABILITY'S
*************************************************************************/
  double P0(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double *dims);//prob of no ionising sources in a volume, n is average number density of sources.
  double Pmin1(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double *dims);//prob of at least one ionising source in a volume, n is average number density of sources.

  // 2 POINT STATS
  double averagebox(); /*Calulate the average value of box*/
  double P2pt_xH(double dim[3]); // ionisation correlation eqn17 & 19 FZH04 

/*************************************************************************
     AUXILLARIES
*************************************************************************/
  // Integration functions
  double intVdndm(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double dims[3], double z1, double mMin);
  // For dealing with the permutation juggling of each element of P3pt
  double P2perm(double i, double j, double k, double *dims);
  double P3perm(double i, double j, double k, double *dims);
  double P4perm(double i, double j, double k, double *dims);
  double P5perm(double i, double j, double k, double *dims);

  // Volumes
  double V1sphere(double R);
  double V2sphere(double r1, double r2, double d);
  double V3sph_Pow(double a2, double b2, double g2,  double aa, double bb, double cc, double w); //Vol of 3 sphere overlap by Powell, requires common intersection point 
  double V3sph_Gib(double Ra, double Rb, double Rc,  double a, double b, double c); //generalisation of above to allow for no common intersection

/*************************************************************************
     SETTING/ACCESSING CLASSES PRIVATE VARIABLES
 *************************************************************************/

  //SET
  void set_dims(double i, double j, double k); //used to change the indexing of dim array.
  void setn(double n1);
  void setMass(double mass);

  //ACCESS
  void get_dim(double *dims);
  int getRes();
  float getL();
  int getType();
  double getn();
  double getMass();


 protected:
  FILE *DBUG;
  float *BOX, L1;
  double N1, *DIM1, MASS;
  Tocmfast *T;
  Reionise *R;
  Cosmology *C;
  int RES1, BOXTP1;

};

/*************************************************************************
    Integral functions
*************************************************************************/
double intVdndm_fn(double mass, void *p);
struct intVdndm_params {Reionise *r1; Stat3pt *s1; double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1); double *dims;};

/***********************************************************************
    Volume wrapper functions to pass to probability calculations
************************************************************************/
// 3 point: 
double Vdouble(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume in double overlap of 2 spheres minus triple overlap
double VdoubTot(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume in double overlap of 2 spheres
double Vtriple(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume in triple overlap of three spheres
double Vind(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume of a sphere not overlapping with any other
double V2doub(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume in overlapping region of one sphere with two others
double V3doub(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume of all overlapping regions of 3 spheres 
double Vo3pt(double R, double *dims, Stat3pt *s1, Reionise *r1); // volume of common intersection for all 3 spheres

// 2 point:
double Vdouble2pt(double R, double *dims, Stat3pt *s1, Reionise *r1);
double Vdoub2ptClus(double R, double *dims, Stat3pt *s1, Reionise *r1);
double Vo2pt(double R, double *dims, Stat3pt *s1, Reionise *r1);

/*************************************************************************
    General
************************************************************************/
double lawofcos(double a, double b, double theta);

#endif

