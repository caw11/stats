#ifndef _BISPEC_H
#define _BISPEC_H

#include <vector>
#include <fftw3.h>

int calc_bispec(char *file, int res_, int boxtype);
int make_ind_hold(fftwf_complex *box_, std::vector< std::vector<int> > &ind_hold, int res_, int s);

#endif
