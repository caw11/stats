/*
 * ##############   stat3pt.cc   ############### 
 *
 * A class to calculate 3 point statistics for a 3D box
 * of type float and organised into a 1D array according to the following
 * indexing for (x,y,z) box co-ordinates: i = z + D*(y+D*x), 
 * where i is the 1D index and D is the pixel count per box side.
 *
 * Was designed to work with the output of 21CMFAST, but providing the 
 * dataset passed in as (float *box) meets the above requirement it should 
 * be happy.
 *
 * REFERENCES:
 * Furlanetto, Zaldarriaga & Hernquist 2004 (hereafter FZH04)
 * Watkinson & Pritchard 2014 (3 point stats)

DEVELOPMENT NOTES:
Really want BOX to be a pointer to the box pointer living within the main program. 
An alternative i* s to load the data within the class initialisation this halves 
the memory usage, but does mean that the box only exists withing stat3pt and isn't good for integrating with other classes and modules

 */

using namespace std;

#include "STAT3PT.h" 
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

 /***************************************************

            CLASS INITIALISATION/DESTRUCTION

  ***************************************************/

Stat3pt::Stat3pt(FILE *LOG, float *box, int res, float l, int boxtype, Tocmfast *T1, Reionise *R1, Cosmology *c1)
{
  char filename[500];
  
  RES1=res;
  L1=l;
  BOXTP1=boxtype;  
  BOX=(float *) malloc(sizeof(float)*pow(res,3));
  DIM1=(double*) malloc(sizeof(double)*3);
  for (int i=0; i<pow(res,3); i++)
    BOX[i]=box[i];

  T=T1;
  R=R1;
  C=c1;
  DBUG = LOG;
  fprintf(DBUG, "Initialised the Stat3pt class \n");
  sprintf(filename, "mkdir Output_files");
  system(filename);
}

Stat3pt::~Stat3pt()
{
  free(BOX);
  fprintf(DBUG, "Destructor called for Stat3pt class \n");
}


/***************************************************

        ANALYTICAL xH 3-POINT CORRELATION FN

  ***************************************************/

double Stat3pt::P3pt_xH(double a, double b, double theta)
{
  /*
    Returns the probability that 3 points A, B and C (a=BC, b=AC, c=AB)
    are all ionised if ionised bubbles are all spheres with radius R.
    R should be worked out from FZH04 model or some other physically 
    grounded model. 
    Theta should be provided in radians.
  */
  
  float P1, P2, P3, P4, P5;
  //double PVo, PVab, PVac, PVbc;
  double Va, Vb, Vc, Vo, Vab, Vac, Vbc, c;
  double *dims;

  dims=(double*) malloc(sizeof(double)*3);

  /* We are dealing with three exclusive regions that have no 
   overlap with the other spheres. A max of three double overlap 
   regions and possibly a triple overlap region.
  */
  
  c = lawofcos(a,b,theta);
  //cout<<"a,b,c="<<a<<"\t"<<b<<"\t"<<c<<endl;
  /* redundant:
  setn(n); //sets the number density for the probability calculations


  Vo=V3sph_Gib(R,R,R,a,b,c); // triple overlap between A, B and C
  Vab = V2sphere(R,R,c); //overlap between A and B
  Vac = V2sphere(R,R,b); //overlap between A and C
  Vbc = V2sphere(R,R,a); //overlap between B and C
  Va = V1sphere(R);
  Vb = Va;
  Vc = Va;  
  cout<<"Vo ="<<Vo<<"\t Vab ="<<Vab<<"\t Vac ="<<Vac<<"\t Vbc ="<<Vbc<<endl;*/
  if (DEBUG>1)
    fprintf(stderr, "Calculating theoretical 3 point function\n\n........\n");
  // All three points are ionised by 5 different possibilities:

  // 1. One or more source in triple overlap region
  set_dims(a, b, c);
  P1 = Pmin1(&Vo3pt, dims);
  if (DEBUG>1)
    fprintf(stderr, "1. One or more source in triple overlap region p1= %f\n",P1);

  // 2. One or more source in two double overlap regions 
  P2=P2perm(a,b,c,dims);
  if (DEBUG>1)
    fprintf(stderr, "2. One or more source in two double overlap regions p2= %f\n",P2);

  // 3. One or more source in one an exclusive region and also in the opposing double overlap region
  P3=P3perm(a,b,c,dims);
  if (DEBUG>1)
    fprintf(stderr, "3. One or more source in one an exclusive region and also in the opposing double overlap region P3= %f\n",P3);

  // 4. One or more source in each sphere's exclusive region
  P4 = P4perm(a,b,c,dims);
  if (DEBUG>1)
    fprintf(stderr, "4. One or more source in each sphere's exclusive region P4= %f\n",P4);

  // 5. One or more source in every double overlap region
  P5 = P5perm(a,b,c,dims); 
  if (DEBUG>1)
    fprintf(stderr, "5. One or more source in every double overlap region P5=%f\n",P5);
  
  return P1+P2+P3+P4+P5;
}//*/

double Stat3pt::P2perm(double i, double j, double k, double *dims)
{  
  //the dimensions should be passed through in their natural order i.e. a,b,c
  double A, B, C;
  
  /*P2 = Pmin1(Vab - Vo)*Pmin1(Vbc - Vo)*P0(Vac);
  P2+= Pmin1(Vbc - Vo)*Pmin1(Vac - Vo)*P0(Vab);
  P2+= Pmin1(Vac - Vo)*Pmin1(Vab - Vo)*P0(Vbc);*/

  set_dims(i, j, k);
  A = Pmin1(&Vdouble, dims);
  set_dims(j, k, i);
  A*= Pmin1(&Vdouble, dims);
  set_dims(k, i, j);
  A*= P0(&VdoubTot, dims) ;
  
  B = Pmin1(&Vdouble, dims) ;
  set_dims(j, k, i);
  B*= Pmin1(&Vdouble, dims);
  set_dims(i, j, k);
  B*= P0(&VdoubTot, dims);

  C = Pmin1(&Vdouble, dims);  
  set_dims(k, i, j);
  C*= Pmin1(&Vdouble, dims);
  set_dims(j, k, i);
  C*= P0(&VdoubTot, dims);

  return A + B + C;
}  

double Stat3pt::P3perm(double i, double j, double k, double *dims)
{ 
  double A, B, C;

  //the dimensions should be passed through in their natural order i.e. a,b,c
  /*P3 Pmin1(Vab - Vo)*Pmin1(Vc - Vac - Vbc + Vo)*P0(Vac + Vbc - Vo);
  P3+= Pmin1(Vac - Vo)*Pmin1(Vb - Vbc - Vab + Vo)*P0(Vbc + Vab - Vo);
  P3+= Pmin1(Vbc - Vo)*Pmin1(Va - Vab - Vac + Vo)*P0(Vab + Vac - Vo);*/

  set_dims(i, j, k);
  A = Pmin1(&Vdouble, dims);
  //set_dims(k, i, j);
  A*= Pmin1(&Vind, dims)*P0(&V2doub, dims);

  set_dims(k, i, j);
  B = Pmin1(&Vdouble, dims);
  //set_dims(j, k, i);
  B*= Pmin1(&Vind, dims)*P0(&V2doub, dims);

  set_dims(j, k, i);
  C = Pmin1(&Vdouble, dims);
  //set_dims(i, j, k);
  C*=Pmin1(&Vind, dims)*P0(&V2doub, dims);

  return A + B + C;
}

double Stat3pt::P4perm(double i, double j, double k, double *dims)
{
  /*  
      P4 = Pmin1(Va - Vab - Vac + Vo)*Pmin1(Vb - Vbc - Vab + Vo);
      P4*= Pmin1(Vc - Vac - Vbc + Vo)*P0(Vab + Vac + Vbc - 2*Vo);
*/
  double A;
  
  set_dims(i, j, k);
  A = Pmin1(&Vind, dims);

  set_dims(j, k, i);
  A*= Pmin1(&Vind, dims);
 
  set_dims(k, i, j);
  A*= Pmin1(&Vind, dims);
  A*=P0(&V3doub, dims);

  return A;  
}

double Stat3pt::P5perm(double i, double j, double k, double *dims)
{
  /*
  Pmin1(V12-Vo)*Pmin1(V23-Vo)*Pmin1(V13-Vo)*P0(Vo);
  */
  double A;

  set_dims(i, j, k);
  A = Pmin1(&Vdouble, dims);
  set_dims(j, k, i);
  A*= Pmin1(&Vdouble, dims);
  set_dims(k, i, j);
  A*= Pmin1(&Vdouble, dims)*P0(&Vo3pt, dims);

  return A;  
}
double Stat3pt::P0(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double *dims)
{
  /*
    Returns the probability that a volume V contains no ionising
    sources given the average number density of sources.
    This assumes a poissonian distribution of sources.
  */

  return exp(-1.0*intVdndm(Vfn, dims, T->getz(), -1));
  /* Note that we fix use to be for Mmin=coolmass() in the call to intVdndm, 
     but this restriction could be eased by passing a different Mmin in dims.
   */
}
double Stat3pt::Pmin1(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double *dims)
{
  /*
    Returns the probability of an ionised bubble able to
    ionise our point, uses FZH04 model.
  */
  return 1.0 - P0(Vfn, dims);
}

/***************************************************
                  2 POINT STATS
***************************************************/

double Stat3pt::P2pt_xH(double dim[3])
{
  
  /*
    This calculates the 2 point correlation function for the
    ionisation field according to FZH04
   */
  double P, a, b, c;
  double *dims;

  dims=(double*) malloc(sizeof(double)*3);

  a=dim[0];
  b=dim[1];
  c=dim[2];

  set_dims(a,b,c);
  P=Pmin1(&Vo2pt,dims);

  if (inclClus==1) {
    P+=( P0(&Vo2pt,dims)*Pmin1(&Vdouble2pt, dims)*Pmin1(&Vdoub2ptClus, dims) );
  }
  else {
    P+=( P0(&Vo2pt,dims)*pow(Pmin1(&Vdouble2pt, dims),2.0) );
  }
  return P;
}
    
      
double Stat3pt::averagebox() {

  //Finds the average of real *box

  double ave(0.0);
  long long int count(0), total_pix(0);

  total_pix=pow(getRes(),3);  
  
  for (int i=0; i<total_pix; i++){
    ave += BOX[i]; 	  	    	
    count+=1;
  }

  ave /= ((double)(count));

  if (DEBUG>=4) fprintf(stderr, "\nIn Stat3pt::averagebox \n average = %f, boxtype = %i, count = %i\n",ave, getType(), int(count));
  return ave;
} //end averagebox



/***************************************************
                 AUXILLARIES
  ***************************************************/


// Integrals:
 double Stat3pt::intVdndm(double (*Vfn)(double, double[3], Stat3pt *s1, Reionise *r1), double dims[3], double z1, double mMin)
{
  /* If mMin < 0.0, assumes it is 
   * minimum cooling mass (this is an optional parameter).
   *
   * factor of Mscale is a hack to make the integrator deal with 
   * the large masses involves... otherwise just whinges that 
   * the function doesn't diverge.   */

  double result, error, Q;
  double rel_tol (1.0e-4), tol (1.0e-6); //<- relative tolerance
  double size_lmt (1.0e3);  

  gsl_function F;
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (size_lmt);

  //shut off the default error handler
  gsl_error_handler_t * pDefaultHandler; 
  const char * gsl_strerror (const int gsl_errno);
  pDefaultHandler= gsl_set_error_handler_off(); //we have to turn this off because it breaks down as the integral heads to zero (which in this situation is perfetly acceptable).

  struct intVdndm_params params = {R, this, Vfn, dims};
  F.params = &params;
  F.function = &intVdndm_fn;

  if (mMin < 0.0)
    mMin = coolMass(C, z1);
  mMin*=ZETA;
  mMin/=Mscale;
  int status =  gsl_integration_qagiu(&F, mMin, 0, rel_tol, size_lmt , w, &result, &error); 
  if (status) { /* an error occurred */
    if (DEBUG>1)
      fprintf(stderr, "integration error returned: %s ....  \n If it transpires to be for any other reason than the integration being zero program will abort at the next step\n", gsl_strerror (status));

    /* status value specifies the type of error */      
    if ( !(-1.0*tol<=result)&& !(result<=tol) ) {
      fprintf(stderr,"error: %s .... ABORTING!!! \n", gsl_strerror (status));
      return -1;
    }
  }
  gsl_integration_workspace_free (w);
  //cout<<"result = "<<result<<endl;
  Q=ZETA*C->fCollPSExact(R->getz(),-1);
  //cout<<"ionised fraction="<<Q<<"\t"<<(-log(1.0-Q)/Q)<<endl;

  return result*(-log(1.0-Q)/Q);//cw minus 1 deals with addition of one in integral avoiding problems when the prob tends to zero
}

double intVdndm_fn(double mass, void *p)
{
  /*
    This is the function that is to be integrated by intVdndm
    which returns the V(m)*(ionized bubble mass function)

    Factor of  is a hack to make the integrator deal with 
    the large masses involves... otherwise just whinges that 
    the function doesn't diverge.*/

  struct intVdndm_params * params = (struct intVdndm_params *)p;

  double (*Fn)(double, double[3], Stat3pt *, Reionise *) = (params->Vfn);
  double *d = (params->dims);
  Reionise *r = (params->r1);
  Stat3pt *s = (params->s1);
  double ans;

  mass*=Mscale;
  s->setMass(mass); //this is used when clustering is being considered as the excess prob a point is ionised given another is requires mass info.
  //cout<<R->RfromM(mass)<<"\t mass="<<mass<<endl;
  ans=r->dndlmFZH(mass)/mass*Fn(r->RfromM(mass),d, s, r);
  //ans = c->dndlM(z,mass)/mass; //to test against nCollObject in dcosmo
  //cout<<"\t mass"<<mass<<"\t qags->  "<<ans*Mscale<<endl;
  return ans*Mscale; 
}

// Volume auxillaries

double Stat3pt::V1sphere(double R)
{
  return 4.0/3.0*PI*pow(R,3.0);
}

double Stat3pt::V2sphere(double r1, double r2, double d)
{
  /*
    Returns the overlap volume of two spheres of radius r1 and r2
    separated by d. 
    Wolfram Alpha 
    (Kern, W. F. and Bland, J. R. Solid Mensuration with Proofs, 
    2nd ed. New York: Wiley, p. 97, 1948.
    Sloane, N. J. A. Sequence A133749 in "The On-Line Encyclopedia 
    of Integer Sequences."
   */
  double V;
  if (d>(r1+r2))
    V=0;//spheres do not overlap
  else {
    V=PI*pow( (r1+r2-d) ,2.0)*(pow(d,2.0) + 2*d*r2 - 3.0*pow(r2,2.0) + 2.0*d*r1 + 6.0*r2*r1 - 3.0*pow(r1,2.0));
    V/=12.0*d;
  }

  return V;
}

double Stat3pt::V3sph_Pow(double a2, double b2, double g2,  double aa, double bb, double cc, double w)
{
  /*
    Calculates the overlap volume of 3 spheres centred at A, B and C
    with radius Ra, Rb and Rc respectively. Sphere centres are separated by
    a=BC, b=CA and c=AB.
    Theta should be provided in radians.
    Reference: Gibson & Scheraga, J. Phys. Chem, 1987, 91, 4121-4122
   */

  double e1, e2, e3, q1,q2,q3;
  double invtan[9], V;

  //cout<<"aa="<<aa<<"\t bb="<<bb<<"\t cc= "<<cc<<endl;
  e1 = (b2 - g2)/aa;
  e2 = (g2 - a2)/bb;
  e3 = (a2 - b2)/cc;
  //cout<<"e1="<<e1<<"\t e2="<<e2<<"\t e3= "<<e3<<endl;

  q1 = sqrt(aa)*( bb + cc - aa + b2 + g2 - 2.0*a2 + e1*(bb - cc) );
  q2 = sqrt(bb)*( cc + aa - bb + g2 + a2 - 2.0*b2 + e2*(cc - aa) );
  q3 = sqrt(cc)*( aa + bb - cc + a2 + b2 - 2.0*g2 + e3*(aa - bb) );

  invtan[0] = atan(2.0*w/q1);
  //cout<<"default arctan1 in units of PI"<<invtan[0]/PI<<endl;
  if (invtan[0] < 0.0)
    invtan[0]+= PI;
  //cout<<"adjusted arctan1 in units of PI"<<invtan[0]/PI<<endl;

  invtan[1] = atan(2.0*w/q2);
  if (invtan[1] < 0.0)
    invtan[1]+= PI;

  invtan[2] = atan(2.0*w/q3);
  if (invtan[2] < 0.0)
    invtan[2]+= PI;

  invtan[3] = atan( sqrt(bb)*w/sqrt(a2)/q2*(1.0 - e2) ); 
  if (invtan[3] < 0.0)
    invtan[3]+= PI;

  invtan[4] = atan( sqrt(cc)*w/sqrt(a2)/q3*(1.0 + e3) ); 
  if (invtan[4] < 0.0)
    invtan[4]+= PI;

  invtan[5] = atan( sqrt(cc)*w/sqrt(b2)/q3*(1.0 - e3) ); 
  if (invtan[5] < 0.0)
    invtan[5]+= PI;

  invtan[6] = atan( sqrt(aa)*w/sqrt(b2)/q1*(1.0 + e1) );
  if (invtan[6] < 0.0)
    invtan[6]+= PI;
 
  invtan[7] = atan( sqrt(aa)*w/sqrt(g2)/q1*(1.0 - e1) ); 
  if (invtan[7] < 0.0)
    invtan[7]+= PI;

  invtan[8] = atan( sqrt(bb)*w/sqrt(g2)/q2*(1.0 + e2) ); 
  if (invtan[8] < 0.0)
    invtan[8]+= PI;	

  V = w/6.0;
  V-= sqrt(aa)/2.0*( b2 + g2 - aa*(1.0/6.0 - pow(e1,2.0)/2.0) )*invtan[0];
  V-= sqrt(bb)/2.0*( g2 + a2 - bb*(1.0/6.0 - pow(e2,2.0)/2.0) )*invtan[1];
  V-= sqrt(cc)/2.0*( a2 + b2 - cc*(1.0/6.0 - pow(e3,2.0)/2.0) )*invtan[2];
  V+= 2.0/3.0*pow(a2,1.5)*(invtan[3] + invtan[4]);
  V+= 2.0/3.0*pow(b2,1.5)*(invtan[5] + invtan[6]);
  V+= 2.0/3.0*pow(g2,1.5)*(invtan[7] + invtan[8]);

  //cout<<"volume and w in powell = "<<V<<"\t"<<w<<endl;


  //cout<<"R="<<a2<<"\t"<<b2<<"\t"<<g2<<endl;
  //cout<<"aa="<<aa<<"\t bb="<<bb<<"\t cc= "<<cc<<endl;
  return V;
}

double Stat3pt::V3sph_Gib(double Ra, double Rb, double Rc,  double a, double b, double c)
{
  /*
    Gibson & Scheraga,J. Phys. Chem, 1987, 91, 4121-4122 [GS87]
    Extension of Powell technique for calculating the volume of 3 overlapping spheres when there is no common intersection.
    We note that this only works when all three spheres overlap in some manner.
    i.e. it can't cope with any one or more spheres not overlapping the
    others. Our over master `if' statement deals with this.
   */
  double a2, b2, g2, aa, bb, cc ,e1, e2, e3;
  double w2, t, tabg, tbga, tcab, p1, p2, p3, p4, p5, p6;
  double V (0.0), tol(1e-3);

  // [1] At least one sphere does not have overlap with at least one of the others and Gibson procedure fails. i.e. there is no triple overlap!
  if ( (a>=(Rb+Rc))||(b>=(Rc+Ra))||(c>=(Ra+Rb)) ) { 
    if (DEBUG>3)
      fprintf(stderr, "At least one sphere does not overlap with one or more of the others\n R=%f/ %f/ %f, a=%f, b=%f, c=%f\n", Ra, Rb, Rc, a, b, c);
    V=0.0; 
  }
  
  else {
    // Parameters useful for calculating volumes:
    a2 = pow(Ra,2.0);
    b2 = pow(Rb,2.0);
    g2 = pow(Rc,2.0);
    
    aa = pow(a,2.0);
    bb = pow(b,2.0);
    cc = pow(c,2.0);
    
    e1 = (b2 - g2)/aa;
    e2 = (g2 - a2)/bb;
    e3 = (a2 - b2)/cc;
    
    w2 = (a2*aa + b2*bb + g2*cc)*(aa + bb + cc); 
    w2-= 2.0*( a2*pow(aa,2.0) + b2*pow(bb, 2.0) + g2*pow(cc,2.0) );
    w2+= aa*bb*cc*(e1*e2 + e2*e3 + e3*e1 -1.0); 
    
    // Parameters used for deciding whether have 3 overlapping spheres, 2 overlapping and one independent or all independent:
    t = (a + b + c)*(-a + b + c)*(a - b + c)*(a + b - c);
    t = sqrt(t);
    //cout<<"t= "<<t<<endl;
    tabg = (a + Rb + Rc)*(-a + Rb + Rc)*(a - Rb + Rc)*(a + Rb - Rc);
    tabg = sqrt(tabg);
    //cout<<"tabg= "<<tabg<<Ra<<"\t"<<Rb<<"\t"<<Rc<<"\t"<<"\t"<<a<<"\t"<<b<<"\t"<<c<<endl;
    
    tbga = (b + Rc + Ra)*(-b + Rc + Ra)*(b - Rc + Ra)*(b + Rc - Ra);
    tbga = sqrt(tbga);
    //cout<<"tbga= "<<tbga<<endl;
    
    tcab = (c + Ra + Rb)*(-c + Ra + Rb)*(c - Ra + Rb)*(c + Ra - Rb);
    tcab = sqrt(tcab);
    //cout<<"tcab= "<<tcab<<endl;
    
    p1 = ( pow( (bb - cc + b2 - g2), 2.0) +  pow((t - tabg),2) )/4.0/aa - a2;
    p2 = ( pow( (bb - cc + b2 - g2), 2.0) +  pow((t + tabg),2) )/4.0/aa - a2;

    p3 = ( pow( (cc - aa + g2 - a2), 2.0) +  pow((t - tbga),2) )/4.0/bb - b2;
    p4 = ( pow( (cc - aa + g2 - a2), 2.0) +  pow((t + tbga),2) )/4.0/bb - b2;
    
    p5 = ( pow( (aa - bb + a2 - b2), 2.0) +  pow((t - tcab),2) )/4.0/cc - g2;
    p6 = ( pow( (aa - bb + a2 - b2), 2.0) +  pow((t + tcab),2) )/4.0/cc - g2;
    
    //cout<<p1<<"\t"<<p2<<"\t"<<p3<<"\t"<<p4<<"\t"<<p5<<"\t"<<p6<<endl;
    //cout<<"w^2 = "<<w2<<endl;

//--------------------------------------------------------
    if (w2 > 0) {
      if (DEBUG>2)
	fprintf(stderr,"[2] triple intersection so can use Powell's approach\n");
      V = V3sph_Pow(a2, b2, g2, aa, bb, cc, sqrt(w2)); 
    }
//--------------------------------------------------------
    else if ( (w2 < tol) && (w2 >-1.0*tol)) {
      if (DEBUG>2)
	fprintf(stderr,"[3] spheres intersect at a single  point\n");
      V=0.0;
    }
//--------------------------------------------------------
    else {     
      if ( (p1>0)&&(p2>0)&&(p3>0)&&(p4>0)&&(p5>0)&&(p6>0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[4] no overlap volume/ triple intersection\n");
	V=0.0;
      }
//--------------------------------------------------------
      else if ( (p1<0)&&(p2<0)&&(p3>0)&&(p4>0)&&(p5>0)&&(p6>0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[5] two spheres dwarfed by third and overlap contained in third and Spheres B and C overlap is contained inside sphere A\n");
	V=V2sphere(Rb, Rc, a); 
      } 
      else if ( (p1>0)&&(p2>0)&&(p3<0)&&(p4<0)&&(p5>0)&&(p6>0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[5] two spheres dwarfed by third and overlap contained in third and Spheres A and C overlap is contained inside sphere B\n");
	V=V2sphere(Rc, Ra, b); 
      }
      else if ( (p1>0)&&(p2>0)&&(p3>0)&&(p4>0)&&(p5<0)&&(p6<0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[5] two spheres dwarfed by third and overlap contained in third and Spheres A and B overlap is contained inside sphere C\n");
	V=V2sphere(Ra, Rb, c);
      }
 //--------------------------------------------------------
      else if ( (p1>0)&&(p2>0)&&(p3<0)&&(p4<0)&&(p5<0)&&(p6<0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[6] one circle inside two other larger circles and Spheres A is inside spheres B and C\n");
	V = V2sphere(Ra, Rb, c) + V2sphere(Rc, Ra, b) - V1sphere(Ra);
      }
      else if ( (p1<0)&&(p2<0)&&(p3>0)&&(p4>0)&&(p5<0)&&(p6<0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[6] one circle inside two other larger circles and Spheres B is inside spheres A and C\n");
	V = V2sphere(Rb, Rc, a) + V2sphere(Ra, Rb, c) - V1sphere(Rb);
      }
      else if ( (p1<0)&&(p2<0)&&(p3<0)&&(p4<0)&&(p5>0)&&(p6>0) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[6] one circle inside two other larger circles and Spheres C is inside spheres A and B\n");
	V = V2sphere(Rc, Ra, b) + V2sphere(Rb, Rc, a) - V1sphere(Rc);
      }
//--------------------------------------------------------
      else if ( (a<Ra) && (b<Ra) && (c<Ra) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[7] Two of the spheres are entirely bound by the third and Spheres A engulfs spheres B and C\n");
	V = V2sphere(Rb, Rc, a);
      }
      else if ( (a<Rb) && (b<Rb) && (c<Rb) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[7] Two of the spheres are entirely bound by the third and Spheres B engulfs spheres A and C\n");
	V = V2sphere(Rc, Ra, b);
      }
      else if ( (a<Rc) && (b<Rc) && (c<Rc) ) {
	if (DEBUG>2)
	  fprintf(stderr,"[7] Two of the spheres are entirely bound by the third and Spheres C engulfs spheres A and B\n");
	V = V2sphere(Ra, Rb, c);
      }
//------------------------------------------------------------
      else if ( (a<(Rb+Rc)) && ((c+Ra)<Rb) && ((b+Ra)<Rc) ){
	if (DEBUG>2)
	  fprintf(stderr,"[8] One sphere is completely inside the overlap of the other two and Sphere A is immersed in overlap region of B and C\n");
	V = V1sphere(Ra);
      }
      else if ( (b<(Rc+Ra)) && ((a+Rb)<Rc) && ((c+Rb)<Ra) ){
	if (DEBUG>2)
	  fprintf(stderr,"[8] One sphere is completely inside the overlap of the other two and Sphere B is immersed in overlap region of A and C\n");
	V = V1sphere(Rb);
      }
      else if ( (c<(Ra+Rb)) && ((b+Rc)<Ra) && ((a+Rc)<Rb) ){
	if (DEBUG>2)
	  fprintf(stderr,"[8] One sphere is completely inside the overlap of the other two and Sphere C is immersed in overlap region of B and A\n");
	V = V1sphere(Rc);
      }
//------------------------------------------------------------ 
      else if ( (a<(Rb+Rc)) && ((c+Ra)<Rb) && ((b+Ra)>Rc) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere A is completely contained in either B and only partially overlaps into C\n");
	  V = V2sphere(Ra, Rc, b);	
      }
      else if ( (a<(Rb+Rc)) && ((c+Ra)>Rb) && ((b+Ra)<Rc) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere A is completely contained in C and only partially overlaps into B\n");
	  V = V2sphere(Ra, Rb, c);	
      }
      else if ( (b<(Rc+Ra)) && ((a+Rb)<Rc) && ((c+Rb)>Ra) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere B is completely contained in either C and only partially overlaps into A\n");
	  V = V2sphere(Rb, Ra, c);	
      }
      else if ( (b<(Rc+Ra)) && ((a+Rb)>Rc) && ((c+Rb)<Ra) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere B is completely contained in A and only partially overlaps into C\n");
	  V = V2sphere(Rb, Rc, a);	
      }
      else if ( (c<(Ra+Rb)) && ((b+Rc)<Ra) && ((a+Rc)>Rb) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere C is completely contained in either A and only partially overlaps into B\n");
	  V = V2sphere(Rc, Rb, a);	
      }
      else if ( (c<(Ra+Rb)) && ((b+Rc)>Ra) && ((a+Rc)<Rb) ){
	if (DEBUG>2)
	  fprintf(stderr,"[9] One sphere completely inside one of the other spheres but  partially overlaps the third sphere and Sphere C is completely contained in B and only partially overlaps into A\n");
	  V = V2sphere(Rc, Ra, b);	
      }
      else {
	fprintf(stderr,"[10] PROBLEM!!! We meet not a single condition for overlap?!? \nw = %e \n Ra =%f, Rb=%f, Rc=%f\n a=%f, b=%f, c=%f. \np1-5= %f\t%f\t%f\t%f\t%f \n ABORTING.....\n",w2, Ra, Rb, Rc, a, b, c, p1, p2, p3, p4, p5);
	return -1; 
      } 
    }
  }
  //cout<<"volume in gibson = "<<V<<endl;
  return V;

}


/***************************************************

         SETTING CLASSES PRIVATE VARIABLES

***************************************************/
 void Stat3pt::set_dims(double i, double j, double k)
{
  DIM1[0]=i;
  DIM1[1]=j;
  DIM1[2]=k;

  //cout<<DIM1[0]<<"\t"<<DIM1[1]<<"\t"<<DIM1[2]<<endl;
}

void Stat3pt::setn(double n1)
{
  N1=n1;
}

void Stat3pt::setMass(double mass)
{
  MASS=mass;
}

 /***************************************************

        ACCESS TO CLASSES PRIVATE VARIABLES

  ***************************************************/

void Stat3pt::get_dim(double *dims)
{
  dims[0]=DIM1[0];
  dims[1]=DIM1[1];
  dims[2]=DIM1[2];
}
int Stat3pt::getRes()
{
  return RES1;
}

float Stat3pt::getL()
{
  return L1;
}

int Stat3pt::getType()
{
  return BOXTP1;
}

double Stat3pt::getn()
 {
   return N1;
 }

double Stat3pt::getMass()
{
  return MASS;
}


double lawofcos(double a, double b, double theta)
{
  /*
    Calculates length of 3rd side for a triangle formed
    of sides a, b and c where a and b are separated by an angle 
    of theta (in radians). 0<theta<pi
   */

  if ( (theta<=0)||(theta>=PI) ) 
    {
      fprintf(stderr,"****** ERROR in lawofcos !! *******\n The value of theta is outside of the physically restricted range 0<theta<PI/n Aborting........");
      return -1;
    }
  double c;
  c = pow(a,2.0) + pow(b,2.0) - 2.0*b*a*cos(theta);
  c = sqrt(c);
  return c;

}

/***************************************************
Volume wrapper functions that we pass to our probability calculations:
***************************************************/


double Vdouble(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  /*
    Returns the volume of overlap for two spheres of our three spheres, 
    both of radius R separated by dim[0]. 
    This is designed to be passed ton intVdndm which
    integrates over m(R).
   */
  double i, j, k;

  s1->get_dim(dims);
  i=dims[0];
  j=dims[1];
  k=dims[2];

  //cout<<i<<"\t"<<j<<"\t"<<k<<endl;
  return s1->V2sphere(R,R,k) - s1->V3sph_Gib(R,R,R,i,j,k);
}

double VdoubTot(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  return s1->V2sphere(R,R,dims[2]);
}


double Vtriple(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  double V,sep;
  s1->get_dim(dims);
  sep=dims[0];
  /*if (sep<2.0*R) 
    V = 4.0*PI*pow(R,3.0)/3.0 - PI*sep*(pow(R,2.0)-pow(sep,2.0)/12.0);
  else
    V = 0.0;*/
  V=s1->V2sphere(R,R,sep);
  //cout<<V<<"\t"<<R<<endl;
  return V; //this is a hack for the 2 point case, needs updating for three points
}

double Vind(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  /*V3 - V13 - V23 + V0*/
  s1->get_dim(dims);
  return s1->V1sphere(R) - s1->V2sphere(R, R, dims[1]) - s1->V2sphere(R, R, dims[0]) + s1->V3sph_Gib(R, R, R, dims[0], dims[1], dims[2]);
   
}

double V2doub(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  /*V13 + V23 - Vo;*/

  s1->get_dim(dims);
  return s1->V2sphere(R, R, dims[1]) + s1->V2sphere(R, R, dims[0]) - s1->V3sph_Gib(R, R, R, dims[0], dims[1], dims[2]); 
}

double V3doub(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  /*Vab + Vac + Vbc - 2*Vo;*/

  s1->get_dim(dims);
  //cout<<"R="<<R<<endl;
  //cout<<"triple overlap is:"<<s1->V3sph_Gib(R, R, R, dims[0], dims[1], dims[2])<<"V1="<<s1->V2sphere(R, R, dims[2])<<"V2="<<s1->V2sphere(R, R, dims[1])<<"V3="<<s1->V2sphere(R, R, dims[0])<<endl;

  return s1->V2sphere(R, R, dims[2]) + s1->V2sphere(R, R, dims[1]) + s1->V2sphere(R, R, dims[0])-(2.0*s1->V3sph_Gib(R, R, R, dims[0], dims[1], dims[2]));
}

double Vo3pt(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  double result;

  s1->get_dim(dims);
  result=s1->V3sph_Gib(R, R, R, dims[0], dims[1], dims[2]);
  //cout<<result<<endl;

  if (result>(4.0/3.0*PI*R*R*R*1.02)) {
    fprintf(stderr, "problem with the overlap volume!!\n separation of points defined by triangle with side of length %.2f, %.2f, %.2f\n The Gibson algorithm calculates the volume to be %f, it should be no more that the volume of one sphere i.e. %f \n ABORTING......\n", dims[0], dims[1], dims[2], result, 4.0/3.0*PI*R*R*R); 
    return -1;
  }
  else if ( result>(4.0/3.0*PI*R*R*R) ) {
    fprintf(stderr, "volume within 2 percent of volume of a sphere; setting the volume from %.2f to that of a single sphere of radius %.2f\n",result, 4.0/3.0*PI*R*R*R);
    result=4.0/3.0*PI*R*R*R;
  }
  return result;
}

double Vdouble2pt(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  double V, sep;
  s1->get_dim(dims); //retrieve previously set dimensional info and load in into our empty dims pointer
  sep = dims[0];

  V = s1->V1sphere(R)- s1->V2sphere(R,R,sep); 
  //make correction for clustering of ionised regions
  V*=(1 + 0);
  
  return V;
}

double Vdoub2ptClus(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  double V, sep;
  s1->get_dim(dims); //retrieve previously set dimensional info and load in into our empty dims pointer
  sep = dims[0];

  V = ( s1->V1sphere(R)- s1->V2sphere(R,R,sep) );
  //make correction for clustering of ionised regions
  V*= ( 1.0-r1->clusCorr(s1->getMass(),sep) ); 

  return V;
}

double Vo2pt(double R, double *dims, Stat3pt *s1, Reionise *r1)
{
  double V,sep;
  s1->get_dim(dims);
  sep=dims[0];
  V=s1->V2sphere(R,R,sep);
  return V;
}

